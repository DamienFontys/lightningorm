﻿namespace LightningORM.QueryStatements
{
    public class WhereClause
    {
        public string Column { get; private set; }
        public string Operator { get; private set; } = "=";
        public string Value { get; private set; }

        public WhereClause(string column, string value)
        {
            Column = column;
            Value = value;
        }

        public WhereClause(string column, string op, string value)
        {
            Column = column;
            Operator = op;
            Value = value;
        }
    }
}
