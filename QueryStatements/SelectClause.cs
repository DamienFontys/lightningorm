namespace LightningORM.QueryStatements
{
    public class SelectClause
    {
        public string Table { get; private set; }
        public string Column{ get; private set; }

        public SelectClause(string table, string column)
        {
            Table = table;
            Column = column;
        }
    }
}