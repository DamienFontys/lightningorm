﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using LightningORM.QueryStatements;

namespace LightningORM
{
    public class QueryBuilder<T>
    {
        public string TableName { get; private set; }
        
        List<SelectClause> QuerySelect = new List<SelectClause>();
        List<WhereClause> QueryWhere = new List<WhereClause>();
        int QueryLimit = 0;

        public QueryBuilder(string tableName)
        {
            TableName = tableName;

        }
        
        public QueryBuilder<T> Select(string column)
        {
            QuerySelect.Add(new SelectClause(TableName, column));
            return this;
        }

        public QueryBuilder<T> Where(string column, string value)
        {
            QueryWhere.Add(new WhereClause(column, value));
            return this;
        }

        public QueryBuilder<T> Where(string column, string op, string value)
        {
            QueryWhere.Add(new WhereClause(column, op, value));
            return this;
        }

        public QueryBuilder<T> Where(WhereClause whereClause)
        {
            QueryWhere.Add(whereClause);
            return this;
        }

        public QueryBuilder<T> Limit(int limit)
        {
            QueryLimit = limit;
            return this;
        }
        
        public List<T> Get()
        {
            var models = new List<T>();
            
            foreach (var result in Database.Instance.Query(GetQueryString(), null))
            {
                var model = (T)Activator.CreateInstance(typeof(T), result);
                models.Add(model);
            }
            
            return models;
        }

        public T First()
        {
            return Get().Count > 0 ? Get().First() : default(T);
        }

        public List<T> All()
        {
            var models = new List<T>();
            foreach (var result in Database.Instance.Query("SELECT * FROM " + TableName, null))
            {
                var model = (T) Activator.CreateInstance(typeof(T), result);
                models.Add(model);
            }

            return models;
        }

        private string GetQueryString()
        {
            var queryString = "";

            queryString += "SELECT ";

            var select = "";
            if (QuerySelect.Count > 0)
            {
                QuerySelect.Add(new SelectClause(TableName, "id"));
                foreach (var selectClause in QuerySelect)
                {
                    select += selectClause.Table.ToLower() + "." + selectClause.Column.ToLower();
                    if (selectClause != QuerySelect.Last())
                    {
                        select += ", ";
                    }
                }
            }
            else
            {
                select = "*";
            }

            queryString += select;

            queryString += " FROM ";

            queryString += TableName;

            //add where clauses
            if (QueryWhere.Count > 0)
            {
                queryString += " WHERE ";
                foreach (var where in QueryWhere)
                {
                    if (where.Value.Contains("%"))
                    {
                        queryString += where.Column + " LIKE " + "'" + where.Value + "'";
                    }else
                    {
                        queryString += where.Column + " " + where.Operator + " '" + where.Value + "'";
                    }
                    

                    if (where != QueryWhere.Last())
                    {
                        queryString += " AND ";
                    }
                    
                }
            }
            
            if (QueryLimit > 0)
            {
                queryString += " LIMIT " + QueryLimit; 
            }

            queryString += ";";

            return queryString;
        }
    }
}
