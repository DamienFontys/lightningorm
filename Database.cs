using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;

namespace LightningORM
{
    public sealed class Database
    {
        private static readonly Lazy<Database> database = new Lazy<Database>(() => new Database());

        public static Database Instance
        {
            get { return database.Value; }
        }

        private MySqlConnection DatabaseConnection;

        static Database()
        {
        }

        public void SetConnection(string connectionString)
        {
            this.DatabaseConnection = new MySqlConnection(connectionString);
        }

        public void SetConnection(string host, string username, string password, string database)
        {
            this.SetConnection("server=" + host + ";user id=" + username + ";password=" + password + ";database=" +
                               database);
        }

        private void OpenConnection()
        {
            try
            {
                DatabaseConnection.Open();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw new Exception("Could not connect to database");
            }
        }

        private void CloseConnection()
        {
            try
            {
                DatabaseConnection.Close();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw new Exception("Could not disconnect from database");
            }
        }

        public List<QueryResult> Query(string query, string[] parameters)
        {
            List<QueryResult> results = new List<QueryResult>();
            MySqlCommand databaseQuery = new MySqlCommand(query, DatabaseConnection);

            if (parameters != null)
            {
                for (var i = 1; i < parameters.Length + 1; i++)
                {
                    databaseQuery.Parameters.AddWithValue("param" + i, parameters[i - 1]);
                }
            }

            OpenConnection();

            var dataReader = databaseQuery.ExecuteReader();

            while (dataReader.Read())
            {
                QueryResult result = new QueryResult(dataReader);
                results.Add(result);
            }

            CloseConnection();
            return results;
        }

        public int Insert(string query, string[] parameters)
        {
            List<QueryResult> results = new List<QueryResult>();

            MySqlCommand databaseQuery = new MySqlCommand(query, DatabaseConnection);
            if (parameters != null)
            {
                for (var i = 1; i < parameters.Length + 1; i++)
                {
                    databaseQuery.Parameters.AddWithValue("param" + i, parameters[i - 1]);
                }
            }

            OpenConnection();
            databaseQuery.ExecuteNonQuery();

            int id = (int) databaseQuery.LastInsertedId;

            CloseConnection();
            return id;
        }
    }
}