﻿﻿using LightningORM.Relationships;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using LightningORM.QueryStatements;
using MySqlX.XDevAPI.Relational;

namespace LightningORM
{
    public class Model<T> where T : Model<T>
    {
        [Property] public int Id { get; set; }

        private List<PropertyInfo> RawProperties { get; set; } = new List<PropertyInfo>();
        private List<KeyValuePair<string, object>> Properties { get; set; } = new List<KeyValuePair<string, object>>();

        //Constructors

        #region Constructors

        protected Model(QueryResult queryResult)
        {
            Properties = queryResult.Properties;

            RawProperties = RegisteredProperties();

            foreach (var prop in RawProperties)
            {
                var name = prop.Name.ToLower();
                var found = queryResult.Properties.FindAll(p => p.Key == name);

                SetRawProperty(prop, found.First().Value);
            }
        }

        protected Model()
        {
            RawProperties = RegisteredProperties();
        }

        #endregion

        //End Constructors

        public virtual string Table()
        {
            return typeof(T).Name.ToLower();
        }

        //Indexer

        #region Indexer

        public object this[string column]
        {
            get { return Properties.Find(x => x.Key == column).Value; }
            set => SetProperty(column, value);
        }

        #endregion

        //EndIndexer

        private void SetRawProperty(PropertyInfo property, object value)
        {
            var type = property.PropertyType.Name.ToLower();

            switch (type)
            {
                case "string":
                    property.SetValue(this, Convert.ToString(value));
                    break;
                case "int32":
                    property.SetValue(this, Convert.ToInt32(value));
                    break;
                default:
                    property.SetValue(this, Convert.ToString(value));
                    break;
            }
        }

        private void SetProperty(string property, object value)
        {
            var pair = new KeyValuePair<string, object>(property, value);

            var index = Properties.FindIndex(x => x.Key == property);

            if (index >= 0)
            {
                Properties[index] = pair;
            }
            else
            {
                Properties.Add(pair);
            }

            var rawProp = RawProperties.Find(p => p.Name.ToLower() == property);

            SetRawProperty(rawProp, value);
        }

        //Database Statements

        #region Database statements

        public static QueryBuilder<T> Select(string column)
        {
            return new QueryBuilder<T>(((T) Activator.CreateInstance(typeof(T))).Table()).Select(column);
        }

        public static QueryBuilder<T> Where(string column, string value)
        {
            return new QueryBuilder<T>(((T) Activator.CreateInstance(typeof(T))).Table()).Where(column, value);
        }

        public static QueryBuilder<T> Where(string column, string op, string value)
        {
            return new QueryBuilder<T>(((T) Activator.CreateInstance(typeof(T))).Table()).Where(column, op, value);
        }

        public static QueryBuilder<T> Where(WhereClause whereClause)
        {
            return new QueryBuilder<T>(((T) Activator.CreateInstance(typeof(T))).Table()).Where(whereClause);
        }

        public static QueryBuilder<T> Limit(int limit)
        {
            return new QueryBuilder<T>(((T) Activator.CreateInstance(typeof(T))).Table()).Limit(limit);
        }

        private List<T> Get()
        {
            return new QueryBuilder<T>(((T) Activator.CreateInstance(typeof(T))).Table()).Get();
        }

        public static T First()
        {
            return new QueryBuilder<T>(((T) Activator.CreateInstance(typeof(T))).Table()).First();
        }

        public static List<T> All()
        {
            return new QueryBuilder<T>(((T) Activator.CreateInstance(typeof(T))).Table()).All();
        }

        public int? Save()
        {
            var properties = "";
            var values = "";
            var propertyValue = "";


            var props = RegisteredProperties();

            foreach (var prop in props)
            {
                properties += prop.Name.ToLower();
                values += "'" + prop.GetValue(this) + "'";
                propertyValue += prop.Name.ToLower() + " = '" + prop.GetValue(this) + "'";

                if (prop == props.Last())
                {
                    continue;
                }

                properties += ", ";
                values += ", ";
                propertyValue += ", ";
            }

            var exists = Properties.FindAll(property => property.Key == "id");

            if (exists.Count > 0)
            {
                var query = "UPDATE " + Table() + " SET " + propertyValue + " WHERE id = " + this["id"];
                Database.Instance.Query(query, null);
            }
            else
            {
                var query = "INSERT INTO " + Table() + " (" + properties + ") VALUES (" + values + ")";
                return Database.Instance.Insert(query, null);
            }

            return null;
        }

        #endregion

        //End Database Statements

        private static List<PropertyInfo> RegisteredProperties()
        {
            return (from t in typeof(T).GetProperties()
                where t.GetCustomAttributes<PropertyAttribute>().Any()
                select t).ToList();
        }
    }
}